// s34 Discussion - Express.js

const express = require("express");

// app - server
const app = express();

const port = 3000;

// Middlewares - software that provides common services and capabilities to applications outside of whats offered by the operating system

// allows your app to read JSON data
app.use(express.json());

// allows your app to read data from any other forms
app.use(express.urlencoded({extended: true}));


// [SECTION] ROUTES
// GET Method
app.get("/greet", (request, response) => {
	response.send("Hello from the /greet endpoint!")
});

// POST Method
app.post("/hello", (request, response) => {
	response.send(`Hello there, ${request.body.firstName} ${request.body.lastName}!`);
});

// Simple registration form
let users = [];

app.post("/signup", (request, response) => {

	if( request.body.username !== '' && request.body.password !== '' ) {
			users.push(request.body);
			response.send(`User ${request.body.username} sucessfully registered!`);
	} else {
		response.send("Please input BOTH username and password.");
	}

});

// Simple change password transaction
app.patch("/change-password", (request, response) => {

	let message;

	for(let i = 0; i < users.length; i++){
		if(request.body.username == users[i].username){
			users[i].password = request.body.password;

			message = `User ${request.body.username}'s password has been updated!`;

			break;

		} else {
			message = "User does not exist!"
		}
	}

	response.send(message);
});

// S34 Activity

/*
	1. Create a GET route that will access the /home route that will print out a simple message.
	2. Process a GET request at the /home route using postman.
	3. Create a GET route that will access the /items route that will retrieve all the users in the mock database.
	4. Process a GET request at the /uitems route using postman.
	5. Create a DELETE route that will access the /delete-item route to remove a user from the mock database.
	6. Process a DELETE request at the /delete-item route using postman.
	7. Export the Postman collection and save it inside the root folder of our application.

*/

app.get("/home", (request, response) => {
	response.send("Welcome to the home page!")
});

app.get("/items", (request, response) => {
	response.send(`All users on the database retrieved! ${request.body}`)
});

app.delete("/delete-item", (request, response) => {
	response.send("User removed from the database!")
});


app.listen(port, () => console.log(`Server running at ${port}`));







